---
title: Spring 2020 Code-a-thon
date: 2020-02-28
author: Joshua Nelson
tag: <span class="tag"> code-a-thon </span> <span class="tag red"> #ThereWillBePizzaToo! </span> 🍕🍕🍕🍕🍕
summary: Join us for the 2020 Spring Code-a-thon!
_SORT: 1000000000
---

<!-- You can and should use HTML in the summary if needed. This will be injected into the template -->

<section class="titlecard blue-green">
  <div class="container">
    <h1> Spring 2020 Code-a-thon </h1>
  </div>
</section>

  <div class="container">

<p class="date"> February 28th, 2020 </p>
<p class="time"> 7:00pm </p>
<p class="location"> Swearingen 1D11 </p>

The Spring 2020 Code-a-thon is on its way!

Join the Association for Computing Machinery (ACM) for a programming competition.
Solve problems, eat pizza, and find internships at our most anticipated event of the semester!

We have 4 different divisions so that you do not have to compete with people of wildly different skill levels.
Once the competition starts, you can click on the links to join the division:

- [145](https://www.hackerrank.com/acmusc-codeathon-2020-spring-145-division), introductory programming questions
- [146](https://www.hackerrank.com/acmusc-codeathon-2020-spring-146-division), data structures questions
- [240](https://www.hackerrank.com/acmusc-codeathon-2020-spring-240-division), algorithms questions
- [350](https://www.hackerrank.com/acmusc-codeathon-2020-spring-350-division), advanced algorithms questions

The code-a-thon runs 24 hours (7 PM - 7 PM),
but you do not have to be present for all of it.
Submitting problems from home is allowed and welcomed,
especially for alumni not in Columbia.
There will be pizza, snacks, and drinks at the event.

If you come in 1st, 2nd, or 3rd place for a division, you will win a prizes.
Prizes have not yet been determined, but in the past have been Arduinos, gift cards,
flash drives, and USB keyboards.

Working in groups is allowed, but groups cannot win prizes.

Note that there is usually a great deal of overlap between questions for divisions.
As such, you can only win prizes for the first division in which you submitted a problem.

Here are the problems from the Fall 2018 Code-a-thon:

- [240 and 350](https://github.com/JamesPC44/USC_UpperDivision_Fall2018/)
- [145 and 146](https://github.com/JamesPC44/USC_LowerDivision_Fall2018/)

And the problems from the Fall 2019 Code-a-thon:

-[240 and 350](https://github.com/justinba1010/USCCodeathon-F19-Upper)
-[145 and 146](https://github.com/jyn514/USCCodeathonF19-Lower)

We hope to see you at the Code-a-thon!

  </div>
