Hello. Any Pages relating to weekly events should go here. To be safe, name the markdown file by the date that the event will ocur on, or if more than one, add a letter after the date. See below for examples.

For and event happening 1st January 1900, => 19000101.md where the date is YYYYMMDD.md
For two events happening on November 7th 2011, => 20111107a.md and 20111107b.md where a letter is appended on the end to differentiate between the events.

Note that this file will be ignored by SSG since the filename begins with an "_".
