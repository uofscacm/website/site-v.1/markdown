---
title: Answers for the Fall Code-a-thon
date: 2019-10-23
author: Justin Baum, Harrison Howell, and Colter Boudinot
tag: <span class="tag"> Code-a-thon </span> <span class="tag red"> #ThereWillBePizzaToo! </span> 🍕🍕🍕🍕🍕
summary: Review of the answers for the code-a-thon questions
---

<section class="titlecard blue-green">
  <div class="container">
    <h1> Code-a-thon Answers </h1>
  </div>
</section>

  <div class="container">

<p class="date"> Wednesday, October 23rd, 2019 </p>
<p class="time"> 7:00pm </p>
<p class="location"> Swearingen 2A17 </p>

This week we will be going over the answers for the Fall 2019 Code-a-thon.
We will also announces prizes and winners. If you're curious how to solve
one of the problems, be sure to come by!

Most of the event will be for the upper-division problems,
but we will cover a few lower-division problems as well.

The presenters will be Harrison Howell, Justin Baum, Colter Boudinot, and Joshua Nelson.

<p class="downloads"> Downloads </p>

If you want to look at the problems early, the lower-division problems are available
<a href="https://github.com/jyn514/USCCodeathonF19-Lower">here</a>.

The upper division problems are not currently public, but when they are,
they will be available <a href="https://github.com/justinba1010/USCCodeathon-F19-Upper/">here</a>.

<hr>

Update: The upper division problems are now public.

  </div>
