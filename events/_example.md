---
title: Event Name Here
date: 0000-00-00
author: Richard Stallman
tag: <span class="tag"> idk put something here. You can color it too. </span> <span class="tag red"> #ThereWillBePizzaToo! </span> 🍕🍕🍕🍕🍕
summary: Put the summary of the event here!
---

<!-- You can and should use HTML in the summary if needed. This will be injected into the template -->

<section class="titlecard blue-green">
  <div class="container">
    <h1> Event Name Here! </h1>
    <!-- This part is optional -->
    <hr>
    <p> Optional Subtitle! </p>
    <!-- END Optional Part -->
  </div>
</section>

  <div class="container">

<p class="date"> Monday, January 1st, 2011 </p>
<p class="time"> 7:00pm </p>
<p class="location"> Swearingen 2A17 </p>

Put a description of the event here, and anything else too!

Make sure to include:
- Date of Event
- Time of Event
- Location of Event
- Who is doing the event
- What the event will be about
  - Include links to presentations and whatnot
- Whether or not Refreshments (read: pizza) will be provided

<p class="downloads"> Downloads </p>

If needed, Downloads should be placed here

<hr>

Should Updates need to be made, such as attaching presentations and files after the fact, they will go under a Horizontal Rule.

  </div>