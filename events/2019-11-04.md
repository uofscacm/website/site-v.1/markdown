---
title: Intro to C
date: 2019-11-04
author: Joshua Nelson
tag: <span class="tag"> C </span> <span class="tag red"> #ThereWillBePizzaToo! </span> 🍕🍕🍕🍕🍕
summary: Learn about syscalls, threading, and memory by touring a web server written in C!
---

<!-- You can and should use HTML in the summary if needed. This will be injected into the template -->

<section class="titlecard blue-green">
  <div class="container">
    <h1> Intro to C </h1>
  </div>
</section>

  <div class="container">

<p class="date"> Wednesday, November 6th, 2019 </p>
<p class="time"> 7:00pm </p>
<p class="location"> Swearingen 2A17 </p>

Learn about syscalls, threading, and memory by touring a web server written in C!
Topics covered will include:

- How to write a C program
- Headers, preprocessing, and imports
- Error handling without exceptions
- Pointers, output parameters, and stack vs. heap vs. static memory
- Basic data structures in C
- How to read man pages
- How to read and write to and from sockets
- Threading (briefly)
- Signals and signal handlers

This talk will be of special interest to students currently in
311 (Operating Systems) or 416 (Networking).

<p class="downloads"> Links </p>

The full source code for the web server can be found
<a href="https://github.com/jyn514/threaded-server">here</a>.

  </div>
