<!-- Because this page has no YAML header, it will not appear in the index. -->

<section class="titlecard blue-green">
  <div class="container">
    <h1> Partners of ACM@USC </h1>
  </div>
</section>

<div class="gridContainer">
  <figure class="g_1x2 white"> <a href="https://www.eff.org/electronic-frontier-alliance/allies"><img src="img/eff.svg"> <figcaption> Electronic Frontier Foundation </figcaption> </a></figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
  <figure class="g_1x1 white"> <figcaption> Hello World 1 </figcaption> </figure>
</div>

