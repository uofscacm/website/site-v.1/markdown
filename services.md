---
title: Services provided by ACM@USC
date: 2019-01-16
author: Brady O'Leary
tag: <span class="tag red"> #ForTheStudents </span> 🍕
summary: Services provided by ACM@USC for students, such as Textbook Sales, and Internships.
---
<section class="titlecard">
  <div class="container">

# Services and things provided by ACM@USC

  </div>
</section>

<section class="flashcard img" style="background-image: url('/img/usc_tcl_stock.jpeg');">
  <div class="container">
    <h1> Used Textbooks from other Students </h1>
    <p> See what textbooks other students are selling! By using this service, you agree to follow the <a href="https://docs.google.com/document/d/1jOTLtYFqirMw7zhNwGainCqKXK_itxHzGUQYMbFPs5g"> Rules </a>.</p>
    <p><a href="https://docs.google.com/spreadsheets/d/1PuWnWZu-actrV9GtHhOu5Xr51Yx2myQAC9xbovcukLc" class="button"> Go to Spreadsheet >> </a></p>
  </div>
</section>
<section class="flashcard">
  <div class="container">
    <h1> Job Listings </h1>
    <p> Sometimes employers want to hire ACM@USC Students. </p>
    <p> <a href="/jobs/" class="button"> Go to Job Postings >> </a> </p>
  </div>
</section>
<section class="flashcard rainbow">
  <div class="container">
    <h1> Links </h1>
    <p> Links to the Personal Blogs of our Members, and other Neat Websites. </p>
    <p> <a href="/links.html" class="button"> Go to Links >> </a> </p>
  </div>
</section>
