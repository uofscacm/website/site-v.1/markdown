---
title: Oak Pointe Elementary Picture
imgpath: img/oak_pointe_2020_spring.jpeg
grid: 1x2
date: 2020-01-27
---
<div class="container">
    <h1> oak_pointe_2020_spring.jpeg </h1>
    <figure>

        <a href="oak_pointe_2020_spring.jpeg"> <img src="oak_pointe_2020_spring.jpeg"></a>
        <figcaption>
            <span class="figure"> oak_pointe_2020_spring.jpeg </span>
            This picture was taken during <a href="/events/2020-01-27.html">our volunteer trip</a> to Oak Pointe Elementary for STEM night.
        </figcaption>
    </figure>
</div>
