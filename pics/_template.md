---
title: Temporary Title
imgpath: {PATH}
grid: 1x1
date: 0000-00-00
---
<div class="container">
    <h1> {NAME} </h1>
    <figure>
        <a href="{NAME}"> <img src="{NAME}"> </a>
        <figcaption>
            <span class="figure"> {NAME}: </span> <mark>Your Description Should Go Here!</mark>
        </figcaption>
    </figure>
</div>
