---
title: Links to Cool Websites
date: 2019-01-31
author: Brady O'Leary
tag: <span class="tag blue"> #CoolLinks </span>
summary: Links to the personal Blogs of our members, and other cool sites as well. Check it out!
---

<section class="titlecard red-orange">
  <div class="container">
    <h1> Cool Links </h1>
  </div>
</section>

  <div class="container">

# Personal Blogs of Members

These links are from current and past members of ACM@USC.

[The Blog of Charles Daniels](http://cdaniels.net/) - Charles Daniels

[Circle Limit](https://nglaeser.github.io/) - Noemi Glaser

[Clay's Code](https://www.clayscode.com/) - Clay Norris

[Jeremy Day's Blog](https://jaday.io/) - Jeremy Day

[Joshua Nelson's Blog](https://jyn514.github.io/) - Joshua Nelson

[Kelevra](https://kelevra.io/) - Kevin Madison

[The Spicy Stew](http://thespicystew.com/) - Kenny Johnson

[Justin Baum's Personal Website](https://justinba1010.github.io/) - Justin Baum

[Art of the Computer Machine](http://arsmachina.net) - Brady O'Leary

# USC Websites

These links are to USC-related Websites.

[USC Cybsersecurity](https://www.usccyber.org/) - USC Cybersecurity Club

[Supplemental Instruction](https://www.sc.edu/about/offices_and_divisions/student_success_center/supplemental-instruction/index.php) - Student Success Center

# Other Websites

These are other websites.

[Hacker News](https://news.ycombinator.com/) - News for Hackers

[Free Software Foundation](https://www.fsf.org/) - The Free Software Foundation Website

[GNU](https://www.gnu.org/) - The GNU Project's Website

[EFF](https://www.eff.org/) - The Electronic Frontier Foundation

[OpenBSD](https://www.openbsd.org/) - OpenBSD Website

[MINIX](http://www.minix3.org/) - MINIX Website

[Old ACM Website](https://web.archive.org/web/*/http://acm.cse.sc.edu/) - Archived Version of the old ACM Websites from 2002 to 2011.

___

If there are any websites that you would like to see here, or you have a personal blog that we don't have listed here, then [Contact the Webmaster](mailto:uofscacm@gmail.com?subject=AddLink).

  </div>
